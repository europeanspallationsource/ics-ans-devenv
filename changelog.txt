2.1.3
    SSSD configuration file modified to work even on a (possible future) CentOS 7.2 upgrade.
    changelog.txt file added

2.1.2

    Centos no longer takes its repositories from a link that has disappeared but from a new stable one (http://vault.centos.org/7.1.1503/).

2.1.1

    Ipython upgrade no longer causing problems with the jira-0.50-py2.7.egg file (IDEBR-27 and IDEBR-29)
    CS-Studio installation skipped if already installed
    openXAL installation skipped if already the latest version
    No more danger of line duplication in auto.nfs (IDEBR-20)

2.1.0

    Software included required by EPICS tutorials users
    Centos repositories fixed to version 7.1, preventing users to update Centos release to 7.2 with corresponding new packages. This will prevent some unexpected problems.

2.0.2

    Version 2.0.1 got broken after in new versions of the pillow package IDEBR-14. Version 2.0.2 fix this problem.
    Use pip instead of easy_install for python package installation (Thanks Yngve Inntjore Levinsen for your pull request).

2.0.1

    Ipython now works on physical machines. It places notebooks in /opt/ipython/notebooks.

2.0.0

The main difference is that instead of having all the provisioning instructions packaged in the Vagrantbox (you have obtained with 'vagrant init'), they are now specified in the Vagrantfile you download. 1.0.0 offers a lot of simplicity but does not give us all the flexibility to address user feedback at these early stages. 2.0.0 will make these backwards incompatible changes more unlikely in the future.
 Other changes
1.0.0

Obsolete
Due to the change in setup instructions and backwards incompatibility of version 2.0.0, the version 1.0.0 is now obsolete.
