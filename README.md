WARNING! THIS REPOSITORY IS DEPRECATED.

Please check https://bitbucket.org/europeanspallationsource/ics-packer-devenv

------------------------------------------------------------------------------

Development Environment is the software and the supporting infrastructure for development at ESS and at in-kind institutes. The main constituents concerning developers are the Development Machine on which all development is to be performed and a centralized boot Server hosting Experimental Physics and Industrial Control System (EPICS) environment accessible by Development Machines and Control Boxes. One Development Machine is expected per user and one server per institute. Both are released in numbered versions to ensure a standardized environment for all users. Servers at different institutes are synchronized one way from ESS outwards to ensure stable versions of EPICS related software to be up-to-date.

Development Machine is a Virtual Machine (VM) or a physical machine on which all development is performed.

Since many developers use EPICS an environment is provided called the ESS EPICS Environment (EEE). EEE does not reside on the Development Machine but is instead placed on a centralized physical EEE server. It is made available to the Development Machines and Control Boxes through a Network File System (NFS) mount which enables them to access the same files on the server without keeping a local copy. EEE server is also a boot server enabling Control Boxes to boot and mount their root file systems from it. Each institute should have one EEE server.

When a new functionality is added to the EEE server at ESS, it should not only be available to ESS but also to in-kind developers. This is achieved by periodic one-way synchronizing of in-kind EEE servers with the ESS EEE server. Synchronization is performed over the internet using Rsync  (Owncloud in versions <= 1.0.3). More specifically, EEE residing on the ESS EEE server is NFS mounted to the Rsync Server which makes it accessible to Rsync client software installed on In-kind EEE servers.

More information here: https://ess-ics.atlassian.net/wiki/display/DE/ICS+Development+Environment

Contact and bug report: devenv-support@esss.se

Bugs can also be reported directly to the [Jira page](https://ess-ics.atlassian.net/projects/IDEBR/)
